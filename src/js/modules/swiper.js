import Swiper from 'swiper'

// Swiper - slider for about section

const galleryThumbs = new Swiper('.gallery-elements', {

  spaceBetween: 10,
  slidesPerView: 3,
  touchRatio: 0.2,
  loop: true,
  loopedSlides: 3,
  slideToClickedSlide: true,
  watchSlidesProgress: true,
});

const gallerySelected = new Swiper('.gallery-selected', {
  spaceBetween: 0,
  touchRatio: 0,
  thumbs: {
    swiper: galleryThumbs
  },
  centeredSlides: true,
});

// END
// Swiper - slider for reviews section 

const swiperReviews = new Swiper('.swiper-reviews-container', {
  slidesPerView: 2,
  spaceBetween: 30,
  touchRatio: 0,
  autoplay: {
    delay: 5500,
  },

breakpoints: {
  1000: {
    slidesPerView: 2,
    spaceBetween: 20
  },
  100: {
    slidesPerView: 1,
    spaceBetween: 0,  
    }
  },


  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
})

//END

// Swiper - slide for full-width-slider

const swipper_full_width = new Swiper('.swipper-full-width', {
  slidesPerView: 5,
  spaceBetween: 30,
  touchRatio: 0,
  breakpoints: {
    1500: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    1100: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    800: {
      slidesPerView: 2,
      spaceBetween: 10,  
    },
    100: {
      slidesPerView: 1,
      spaceBetween: 0,  
    }
  },

  autoplay: {
    delay: 5500,
  },
  
  navigation: {
    nextEl: '.swipper-full-width-next',
    prevEl: '.swipper-full-width-prev',
  },
})

//END
